﻿#include <iostream>


void FindOddNumbers(int Limit)
{
	for (int i = 1; i < Limit; i += 2)
	{
		std::cout << i << "\n";
	}
}

void FindEvenNumbers(int Limit)
{
	for (int i = 0; i < Limit; i += 2)
	{
		std::cout << i << "\n";
	}
}


int main()
{
	FindOddNumbers(15);
	std::cout << "\n";
	FindEvenNumbers(15);
}

/* 
 ПО ЗАДАНИЮ НАДО СДЕЛАТЬ ТАК,НО КАК ПО МНЕ УДОБНО КОГДА РАЗБИТО НА РАЗНЫЕ ФУНКЦИИ,ЧЕТ И НЕ ЧЕТ

void FindOddNumbers(int Limit, bool isodd)
{
	if(isodd)
		for (int i = 1; i < Limit; i += 2)
		{
			std::cout << i << "\n";
		}
	else
	{
		for (int i = 0; i < Limit; i += 2)
		{
			std::cout << i << "\n";
		}
	}
}


int main()
{
	FindOddNumbers(15,true);
	std::cout << "\n";
	FindOddNumbers(15,false);
}
 */